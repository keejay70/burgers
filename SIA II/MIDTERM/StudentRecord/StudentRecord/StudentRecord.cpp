// StudentRecord.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include "pch.h"
#include "stdafx.h"
#include "StudentModel.h"
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <string>
#include <windows.h>
#define MAX 100
#define SIZE 5



using namespace std;	

typedef void(*QuickSort)(int*, int, int);
typedef void(*MergeSort)(int*, int, int);
typedef void(*BubbleSort)(string*, int);
typedef void(*PigHoleSort)(int*,int);
typedef void(*HeapSort)(char*, int);

int main()
{
	int tempInt[5];
	string tempString[5];
	char tempChar[5];

	Student stud_Array[] = { 
		{"Kent", 77, 87, 86, 90, 'A'}, 
		{"Tyronn", 78, 87, 78, 87, 'B'}, 
		{"Aerol", 79, 89, 85 ,66, 'C'}, 
		{"John", 84, 85, 77, 79, 'B'}, 
		{"Mitch", 76, 76, 78, 88, 'B'} 
	};
	int n = sizeof(stud_Array) / sizeof(stud_Array[0]);
	
	QuickSort quick;
	MergeSort merge;
	BubbleSort bubble;
	PigHoleSort pigeon;
	HeapSort heapsss;


	HINSTANCE dll = LoadLibrary(L"C:\\Users\\TyronnOcso\\source\\repos\\MIDTERM\\sortDLL\\Debug\\sortDLL.dll");

	cout << "Records : " << endl;
	printf("Name\t PreMidterm Grade\t Midterm Grade\t\t PreFinal Grade\t\t Final Grade\t\t Final score\n");
	cout<<"---------------------------------------------------------------------------------------------------------------------------\n";
	for (int i = 0; i < n; i++) {
		cout << stud_Array[i].name<<"\t\t" << stud_Array[i].pmGR<<"\t\t\t" << stud_Array[i].mGR << "\t\t     " << stud_Array[i].pfGR << "\t\t\t   " << stud_Array[i].fGR << "\t\t\t   " << stud_Array[i].cG<<"\n";
	}

	if (dll)
	{

		char choice, bol;

		
		quick = (QuickSort)GetProcAddress(dll, "quickSort");
		merge = (MergeSort)GetProcAddress(dll, "mergesort");
		bubble = (BubbleSort)GetProcAddress(dll, "bubbleSort");
		pigeon = (PigHoleSort)GetProcAddress(dll, "pigHoleSort");
		heapsss = (HeapSort)GetProcAddress(dll, "heapSort");
		
		do {
			cout << "A.) Quick sort\nB.) Merge sort\n C.) Bubble sort\nD.) Pigeon Hole\nE.) Heap sort\n";
			cin >> choice;

		switch (choice)
		{
		case 'a':
		case 'A':
			printf("----------------------------------------------------QUICK SORT------------------------------------------");
			for (int i = 0; i < 5; i++) {
				tempInt[i] = stud_Array[i].pmGR;
			}
			

			printf("\n");
			quick(tempInt, 0, 4);
			cout << "Sorted using Quick sort\n";

			for (int i = 0; i < n; i++) {
				cout << tempInt[i] << "\n";
			}
			
			cout << "\n\nWant to proceed? (y/n)";
			cin >> bol;
			if (bol == 'n')
			{
				bol = 'N';
			}
			cout << "\n";
			printf("\n");

		break;
		case 'b':
		case 'B':
			printf("----------------------------------------------------MERGE SORT------------------------------------------");
			for (int i = 0; i < 5; i++) {
				tempInt[i] = stud_Array[i].fGR;
			}
			printf("\n");
			merge(tempInt, 0, n-1);
			cout << "Sorted using Merge Sort \n";
			for (int i = 0; i < n; i++) {
				cout << tempInt[i] << "\n";
			}
			
			cout << "\n";
			cout << "\n\nWant to proceed? (y/n)";
			cin >> bol;
			if (bol == 'n')
			{
				bol = 'N';
			}
			cout << "\n";
		break;		
		case 'c':
		case 'C':
			cout << "---------------------------------------------------Bubble Sort---------------------------------------------------";
			for (int i = 0; i < 5; i++) {
				tempString[i] = stud_Array[i].name;
			}

			printf("\n");
			bubble(tempString, 5);
			cout << "Sorted using Quick sort\n";
	
			for (int i = 0; i < n;i++) {
				cout << tempString[i];
				cout << "\n";
			}
			cout << "\n\nWant to proceed? (y/n)";
			cin >> bol;
			if (bol == 'n')
			{
				bol = 'N';
			}
			cout << "\n";
			printf("\n");

			break;

		case 'd':
		case 'D':
			cout << "---------------------------------------------------Pigeon Hole Sort---------------------------------------------------";
			for (int i = 0; i < 5; i++) {
				tempInt[i] = stud_Array[i].fGR;
			}
			cout << "\n";
			pigeon(tempInt, 5);
			cout << "Sorted using Pigeon hole sort";
			for (int i = 0; i < n; i++) {
				cout << tempInt[i] << "\n";
			}

			cout << "\n\nWant to proceed? (y/n)";
			cin >> bol;
			if (bol == 'n')
			{
				bol = 'N';
			}
			cout << "\n";
			printf("\n");

		case 'e':
		case 'E':
			printf("----------------------------------------------------HEAP SORT------------------------------------------");
			for (int i = 0; i < n; i++) {
				tempChar[i] = stud_Array[i].cG;
			}
			printf("\n");

			heapsss(tempChar, SIZE);
			cout << "Sorted using Heap Sort";

			for (int i = 0; i < n; i++) {
				printf("\n");
				printf("%c ",tempChar[i]);
			}
			printf("\n");
			cout << "\n\nWant to proceed? (y/n)";
			cin >> bol;
			if (bol == 'n')
			{
				bol = 'N';
			}
			cout << "\n";
			printf("\n");

		break;
		default:
				cout << "\n";
				cout << "\n\nWant to proceed? (y/n)";
				cin >> bol;
				if (bol == 'n')
				{
					bol = 'N';
				}
				cout << "\n";
				break;
			}

		} while (bol != 'N');
		FreeLibrary(dll);
		
	}
	else
	{
		cout << "DLL Failed To Load!" << std::endl;
	}
	system("PAUSE");
	return 0;
}

