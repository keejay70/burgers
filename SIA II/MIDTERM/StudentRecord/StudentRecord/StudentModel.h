#pragma once
#include "pch.h"
#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

class Student {
	
public:
	string name;
	int pmGR;
	int mGR;
	int pfGR;
	int fGR;
	char cG;

	Student(string name, int pmGR, int mGR, int pfGR, int fGR, char cG);
	~Student() {

	}
};


//faculty       name         time-in        time-out   tot_Num_hrs
/*ArrayL::ArrayL(string str1, string str2, string str3, int num1) {
	this->str1 = str1;
	this->str2 = str2;
	this->str3 = str3;
	this->num1 = num1;
}*/

//student         name       premid   Midterm     prefi   Final     Converted G

Student::Student(string name, int pmGR, int mGR, int pfGR, int fGR, char cG) {
	this->name = name;
	this->pmGR = pmGR;
	this->mGR = mGR;
	this->pfGR = pfGR;
	this->fGR = fGR;
	this->cG = cG;
}
#pragma once
