#pragma once
#include "pch.h"
#include "stdafx.h"
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <vector>

#define MAX 100

using namespace std;

#ifdef DLL_EXPORT
#define SORTDIR __declspec(dllexport)
#else
#define SORTDIR __declspec(dllimport)
#endif

extern "C"
{
	SORTDIR void quickSort(int[], int, int);
	SORTDIR void mergesort(int[], int,int);
	SORTDIR void bubbleSort(string[], int);
	SORTDIR void pigHoleSort(int[], int);
	SORTDIR void heapSort(int[], int);
}