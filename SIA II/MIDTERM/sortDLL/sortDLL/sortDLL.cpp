// sortDLL.cpp : Defines the exported functions for the DLL application.
#include "pch.h"
#include "stdafx.h"
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <string>
#include <vector>
#include "sotDLL.h"



#define MAX 100

using namespace std;

void swap(int* a, int* b)
{
	int t = *a;
	*a = *b;
	*b = t;
}


/* This function takes last element as pivot, places
   the pivot element at its correct position in sorted
	array, and places all smaller (smaller than pivot)
   to left of pivot and all greater elements to right
   of pivot */


int partition(int S[], int low, int high)
{
	int pivot = S[high];    // pivot 
	int i = (low - 1);  // Index of smaller element 

	for (int j = low; j <= high - 1; j++)
	{
		// If current element is smaller than or 
		// equal to pivot 
		if (S[j]<= pivot)
		{
			i++;    // increment index of smaller element 
			swap(&S[i], &S[j]);
		}
	}
	swap(&S[i + 1], &S[high]);
	return (i + 1);
}

extern "C" {

	/* The main function that implements QuickSort
	 arr[] --> Array to be sorted,
	  low  --> Starting index,
	  high  --> Ending index */

	void quickSort(int S[], int low, int high)
	{
		if (low < high)
		{
			/* pi is partitioning index, arr[p] is now
			   at right place */
			int pi = partition(S, low, high);

			// Separately sort elements before 
			// partition and after partition 
			quickSort(S, low, pi - 1);
			quickSort(S, pi + 1, high);
		}
	}

	// A function to merge the two half into a sorted data.
	void merge(int S[], int l, int m, int r)
	{
		int i, j, k;
		int n1 = m - l + 1;
		int n2 = r - m;

		int *L = new int[n1];
		int *R = new int[n2];

		for (i = 0; i < n1; i++)
			L[i] = S[l + i];
		for (j = 0; j < n2; j++)
			R[j] = S[m + 1 + j];

		i = 0; // Initial index of first subarray 
		j = 0; // Initial index of second subarray 
		k = l; // Initial index of merged subarray 
		while (i < n1 && j < n2)
		{
			if (L[i] <= R[j])
			{
				S[k] = L[i];
				i++;
			}
			else
			{
				S[k] = R[j];
				j++;
			}
			k++;
		}

		while (i < n1)
		{
			S[k] = L[i];
			i++;
			k++;
		}

		while (j < n2)
		{
			S[k] = R[j];
			j++;
			k++;
		}

	}

	// A function to split array into two parts.
	void mergesort(int S[], int l, int r)
	{
		if (l < r)
		{
			// Same as (l+r)/2, but avoids overflow for 
			// large l and h 
			int m = l + (r - l) / 2;

			// Sort first and second halves 
			mergesort(S, l, m);
			mergesort(S, m + 1, r);

			merge(S, l, m, r);
		}

	}

	void bubbleSort(string s[], int n)
	{
		string temp[MAX];
		for (int j = 0; j < n - 1; j++)
		{
			for (int i = j + 1; i < n; i++)
			{
				if ((s[j].compare(s[i])) > 0)
				{
					temp[j] = s[j];
					s[j] = s[i];
					s[i] = temp[j];
				}
			}
		}
	}

	void pigHoleSort(int sa[], int n)
	{
		// Find minimum and maximum values in arr[] 
		int min = sa[0], max = sa[0];
		for (int i = 1; i < n; i++)
		{
			if (sa[i] < min)
			{
				min = sa[i];
			}
			if (sa[i] > max)
			{
				max = sa[i];
			}
		}

		int range = max - min + 1; // Find range 

		// Create an array of vectors. Size of array 
		// range. Each vector represents a hole that 
		// is going to contain matching elements. 
		// int *L = new int[n1];
		//vector<int> holes[range];
		vector<int> *holes = new vector<int>[range];

		// Traverse through input array and put every 
		// element in its respective hole 
		for (int i = 0; i < n; i++) {
			holes[sa[i] - min].push_back(sa[i]);
		}


		// Traverse through all holes one by one. For 
		// every hole, take its elements and put in 
		// array. 
		int index = 0;  // index in sorted array 
		for (int i = 0; i < range; i++)
		{
			vector<int>::iterator it;
			for (it = holes[i].begin(); it != holes[i].end(); ++it)
			{
				sa[index++] = *it;
			}
		}

	}

	void heapify(int s[], int n, int i)
	{
		int largest = i; // Initialize largest as root 
		int l = 2 * i + 1; // left = 2*i + 1 
		int r = 2 * i + 2; // right = 2*i + 2 

		// If left child is larger than root 
		if (l < n && s[l] > s[largest])
			largest = l;

		// If right child is larger than largest so far 
		if (r < n && s[r] > s[largest])
			largest = r;

		// If largest is not root 
		if (largest != i)
		{
			swap(s[i], s[largest]);

			// Recursively heapify the affected sub-tree 
			heapify(s, n, largest);
		}
	}

	// main function to do heap sort 
	void heapSort(int s[], int n)
	{

		// Build heap (rearrange array) 
		for (int i = n / 2 - 1; i >= 0; i--)
			heapify(s, n, i);

		// One by one extract an element from heap 
		for (int i = n - 1; i >= 0; i--)
		{
			// Move current root to end 
			swap(s[0], s[i]);

			// call max heapify on the reduced heap 
			heapify(s, i, 0);
		}
	}

}	