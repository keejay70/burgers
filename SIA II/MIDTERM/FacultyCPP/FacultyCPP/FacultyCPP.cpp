// FacultyCPP.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <stdio.h>
#include <iostream>
#include <windows.h>
#define MAX 100


typedef struct
{
	char name[MAX];
	char time_IN[MAX];
	char time_OUT[MAX];
	int total_Hrs_Worked;
}Faculty;

using namespace std;

typedef void(*PigHoleSort)(Faculty*,int);
typedef void(*BubSort)(Faculty*, int);
typedef void(*BubTimeSort)(Faculty*, int);

int main()
{
	int n = 5;
	Faculty fac_Array[] = { {"Christian Maderazo","7:30 am","6:30 pm",10},{"Gran Sabandal","8:00 am","9:00 pm",13},{"Ange Ceniza","7:00 am","6:00 pm",11},{"Jesse Seva","7:30 am","4:30 pm",9},{"Ken Gorro","6:00 am","1:00 pm",7} };
	
	PigHoleSort pigeonSort;
	BubSort bubble;
	BubTimeSort timeBuble;

	HINSTANCE hInstLibrary = LoadLibrary(L"C:\\Users\\TyronnOcso\\source\\repos\\MIDTERM\\sortDLL\\Debug\\sortDLL.dll");

	cout << "Records : " << endl;
	printf("Name \t\t\t\tTime - IN \t\t\t\t Time-Out \t\t\t\t Total Hours Work\n");
	for (int i = 0; i < n; i++) {
		printf("%s \t\t%s \t\t\t%s \t\t\t%d\n", fac_Array[i].name, fac_Array[i].time_IN, fac_Array[i].time_OUT,fac_Array[i].total_Hrs_Worked);

	}


	if (hInstLibrary)
	{
		char choice,bol;
		pigeonSort = (PigHoleSort)GetProcAddress(hInstLibrary,"pigHoleSort");
		bubble = (BubSort)GetProcAddress(hInstLibrary, "bubSortS");
		timeBuble = (BubTimeSort)GetProcAddress(hInstLibrary, "bubSortSTmin");

		do {
			cout << "A.) Sort Name using bubble sort\nB.) Sort Time using bubble sort\nC.) Sort Hours worked using Pigeonhole sort\n";
			cin >> choice;

			switch (choice)
			{
			case 'a':
			case 'A':cout << "\n----------------------------------------------Bubble Sort------------------------------------------\n";
				bubble(fac_Array, n);
				for (int i = 0; i < n; i++) {
					printf("\n");
					cout << fac_Array[i].name;
				}
				cout << "\n\nWant to proceed? (y/n)";
				cin >> bol;
				if (bol == 'n')
				{
					bol = 'N';
				}
				cout << "\n";
			break;

			case 'b':
			case 'B':cout << "\n----------------------------------------------Bubble Sort------------------------------------------\n";
				timeBuble(fac_Array, n);
				for (int i = 0; i < n; i++) {
					printf("\n");
					cout << fac_Array[i].time_IN;
				}cout << "\n\nWant to proceed? (y/n)";
				cin >> bol;
				if (bol == 'n')
				{
					bol = 'N';
				}
				cout << "\n";
				break;

			case 'c':
			case 'C':printf("\n----------------------------------------Pigeonhole sort---------------------------------------------\n");
				pigeonSort(fac_Array, 5);
				for (int i = 0; i < n; i++) {
					printf("\n");
					printf("%d ", fac_Array[i].total_Hrs_Worked);
				}
				cout << "\n\nWant to proceed? (y/n)";
				cin >> bol;
				if (bol == 'n')
				{
					bol = 'N';
				}
				cout << "\n";
				break;

			default:
				
				break;
			}

		} while (bol != 'N');



		


		
		cout << "\n";

		

		FreeLibrary(hInstLibrary);
	}
	else
	{
		cout << "DLL Failed To Load!" << std::endl;
	}

	system("PAUSE");
	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
