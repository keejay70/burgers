﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortStudentApp
{
    class Student
    {
        //Name, prem, mid, prefi, final, alphagrade
        public string name;
        public int prem;
        public int mid;
        public int pref;
        public int final;
        public char alphaGrade;

        public Student()
        {

        }

        public Student(string name, int prem, int mid, int pref, int final, char alphaGrade)
        {
            this.name = name;
            this.prem = prem;
            this.mid = mid;
            this.pref = pref;
            this.final = final;
            this.alphaGrade = alphaGrade;
        }
    }
}
