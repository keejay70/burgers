﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace SortStudentApp
{
    class Program
    {
        [DllImport("D:\\burgers\\SIA II\\MIDTERM\\sortDLL\\Debug\\sortDLL.dll")]
        public static extern void quickSort(int[] arrayInt, int low, int high);

        [DllImport("D:\\burgers\\SIA II\\MIDTERM\\sortDLL\\Debug\\sortDLL.dll")]
        public static extern void pigHoleSort(int[] arrayInt, int low);

        [DllImport("D:\\burgers\\SIA II\\MIDTERM\\sortDLL\\Debug\\sortDLL.dll")]
        public static extern void bubbleSort(String[] arrayStr, int low);

        [DllImport("D:\\burgers\\SIA II\\MIDTERM\\sortDLL\\Debug\\sortDLL.dll")]
        public static extern void heapSort(int[] arrChar, int low);

        [DllImport("D:\\burgers\\SIA II\\MIDTERM\\sortDLL\\Debug\\sortDLL.dll")]
        public static extern void mergesort(int[] arrInt, int low, int high );
        static void Main(string[] args)
        {
            int i;
            string cont = "y";
            int[] tempInt = new int[10];
            int[] tempChar = new int[10];
            String[] tempString = new String[10];
            Student[] mod = new Student[5];

            mod[0] = new Student("Kent Setenta", 1, 1, 1, 1, 'A');
            mod[1] = new Student("Aerol Eroja", 3, 2, 3, 3, 'C');
            mod[2] = new Student("Tyronn Ocso", 1, 3, 5, 5, 'B');
            mod[3] = new Student("John Sinay", 1, 2, 2, 2, 'A');
            mod[4] = new Student("Mitch Huan", 5, 5, 5, 4, 'F');
            Console.WriteLine("Name \t\t PreMid \tMidterm\tPreFinal\tFinal\tAlpha Grade");
            for (i = 0; i < 5; i++)
            {
                Console.WriteLine(mod[i].name + "\t" + mod[i].prem + "\t\t" + mod[i].mid +"\t"+mod[i].pref +"\t"+ "\t" + mod[i].final + "\t" + mod[i].alphaGrade + "\n");
            }
            do
            {
                Console.WriteLine("-------------------------------------------------" +
                "\n1.) Quick sort" +
                "\n2.) Merge sort" +
                "\n3.) Bubble sort" +
                "\n4.) Pigeon Hole" +
                "\n5.) Heap sort\n");
                switch (Console.ReadLine())
                {
                    case "1":
                        Console.WriteLine("Quick Sort Midterm");
                        for (int j = 0; j < 5; j++)
                        {
                            tempInt[j] = mod[j].mid;
                        }
                        quickSort(tempInt, 0, 4);
                        for (int a = 0; a < 5; a++)
                        {
                            Console.WriteLine(tempInt[a]);
                        }
                        Console.Write("Proceed?(y/n)");
                        cont = Console.ReadLine();
                        if (cont == "n")
                        {
                            cont = "n";
                        }
                        break;
                    case "2":
                        Console.WriteLine("Merge Sort Final");
                        for (int j = 0; j < 5; j++)
                        {
                            tempInt[j] = mod[j].final;
                        }
                        mergesort(tempInt, 0, 4);
                        for (int a = 0; a < 5; a++)
                        {
                            Console.WriteLine(tempInt[a]);
                        }
                        Console.Write("Proceed?(y/n)");
                        cont = Console.ReadLine();
                        if (cont == "n")
                        {
                            cont = "n";
                        }
                        break;
                    case "3":
                        Console.WriteLine("Bubble Sort Name ");
                        for (int b = 0; b < 5; b++)
                        {
                            tempString[b] = mod[b].name;
                        }
                        bubbleSort(tempString, 5);
                        for (int c = 0; c < 5; c++)
                        {
                            Console.WriteLine(tempString[c]);
                        }
                        Console.Write("Proceed?(y/n)");
                        cont = Console.ReadLine();
                        if (cont == "n")
                        {
                            cont = "n";
                        }
                        break;
                    case "4":
                        Console.WriteLine("PigeonHole PreFinal");
                        for (int b = 0; b < 5; b++)
                        {
                            tempInt[b] = mod[b].pref;
                        }
                        pigHoleSort(tempInt, 5);
                        for (int c = 0; c < 5; c++)
                        {
                            Console.WriteLine(tempInt[c]);
                        }
                        Console.Write("Proceed?(y/n)");
                        cont = Console.ReadLine();
                        if (cont == "n")
                        {
                            cont = "n";
                        }
                        break;
                    case "5":
                        Console.WriteLine("Heap Sort AlphaGrade");
                        for (int b = 0; b < 5; b++)
                        {
                            tempChar[b] = mod[b].alphaGrade;
                        }
                        heapSort(tempChar, 4);
                        for (int c = 0; c < 5; c++)
                        {
                            Console.WriteLine(Convert.ToChar(tempChar[c]));
                        }
                        Console.Write("Proceed?(y/n)");
                        cont = Console.ReadLine();
                        if (cont == "n")
                        {
                            cont = "n";
                        }
                        break;
                    default:
                        Console.WriteLine("Error");
                        break;
                }
            } while (cont != "n");
        }
    }
}
