﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortFacultyApp
{
    class Faculty
    {
        public string name;
        public string time_in;
        public string time_out;
        public int noOfHours;

        public Faculty()
        {

        }

        public Faculty(string name, string timeIn, string timeOut, int noOfHours)
        {
            this.name = name;
            this.time_in = timeIn;
            this.time_out = timeOut;
            this.noOfHours = noOfHours;
        }

    }
}
