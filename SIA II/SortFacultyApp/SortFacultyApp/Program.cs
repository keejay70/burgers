﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace SortFacultyApp
{
    class Program
    {
       [DllImport("D:\\burgers\\SIA II\\MIDTERM\\sortDLL\\Debug\\sortDLL.dll")]
        public static extern void quickSort(int[] arrayInt, int low, int high);

        [DllImport("D:\\burgers\\SIA II\\MIDTERM\\sortDLL\\Debug\\sortDLL.dll")]
        public static extern void pigHoleSort(int[] arrayInt, int low);

        [DllImport("D:\\burgers\\SIA II\\MIDTERM\\sortDLL\\Debug\\sortDLL.dll")]
        public static extern void bubbleSort(String[] arrayStr, int low);
        static void Main(string[] args)
        {
            int i;
            string cont = "y";
            int[] tempInt = new int[10];
            int[] tempPG = new int[10];
            String[] tempString = new String[10];
            Faculty[] mod = new Faculty[5];

            mod[0] = new Faculty("Kim Gorro  ", "7:30 am", "10:30 pm", 15);
            mod[1] = new Faculty("Gran Sabandal", "9:30 am", "7:30 pm", 10);
            mod[2] = new Faculty("Ange Ceniza", "6:45 am", "8:30 pm", 14);
            mod[3] = new Faculty("Jesse Seva", "10:30 am", "5:30 pm", 7);
            mod[4] = new Faculty("Ken Gorro", "7:00 am", "2:30 pm", 8);
            Console.WriteLine("Name \t\t Time In \tTime Out\tNo. Of Hours");
            for (i = 0; i < 5; i++) {
                Console.WriteLine(mod[i].name +"\t "+ mod[i].time_in +" \t"+ mod[i].time_out +" \t"+ mod[i].noOfHours + "\n");
            }
            do {
                Console.WriteLine("-------------------------------------------------" +
                "\n1.) Sort Time using bubble sort" +
                "\n2.) Sort Name using bubble sort" +
                "\n3.) Sort Hours worked using Pigeonhole sort" +
                "\nChoose a Sorting Method: ");
                switch (Console.ReadLine())
                {
                    case "1":
                        Console.WriteLine("Bubble Sort Time In ");
                        for (int j = 0; j < 5; j++)
                        {
                            tempString[j] = mod[j].time_in;
                        }
                        //bubbleSort(tempString, 5);
                        for (int a = 0; a < 5; a++)
                        {
                            Console.WriteLine(tempString[a]);
                        }
                        Console.Write("Proceed?(y/n)");
                        cont = Console.ReadLine();
                        if (cont == "n")
                        {
                            cont = "n";
                        }
                        break;
                    case "2":
                        Console.WriteLine("Bubble Sort Name");
                        for (int j = 0; j < 5; j++)
                        {
                            tempString[j] = mod[j].name;
                        }
                        //bubbleSort(tempString, 5);
                        for (int a = 0; a < 5; a++)
                        {
                            Console.WriteLine(tempString[a]);
                        }
                        Console.Write("Proceed?(y/n)");
                        cont = Console.ReadLine();
                        if (cont == "n")
                        {
                            cont = "n";
                        }
                        break;
                    case "3":
                        Console.WriteLine("PigeonHole Sort No. Of Hours ");
                        for (int b = 0; b < 5; b++)
                        {
                            tempPG[b] = mod[b].noOfHours;
                        }
                        pigHoleSort(tempPG, 5);
                        for (int c = 0; c < 5; c++)
                        {
                            Console.WriteLine(tempPG[c]);
                        }
                        Console.Write("Proceed?(y/n)");
                        cont = Console.ReadLine();
                        if (cont == "n")
                        {
                            cont = "n";
                        }
                        break;
                    default:
                        Console.WriteLine("Error");
                        break;
                }
            } while(cont != "n");
        }
    }
}
